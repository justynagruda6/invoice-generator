import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {InvoiceItem} from '../../models/invoice-item.model';
import {NewInvoiceService} from '../../services/new-invoice.service';

interface InvoiceItemForm {
  name: FormControl<string | null>;
  count: FormControl<number | null>;
  price: FormControl<number | null>;
}

@Component({
  selector: 'app-new-invoice-form',
  templateUrl: './new-invoice-form.component.html',
  styleUrls: ['./new-invoice-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewInvoiceFormComponent {
  readonly newInvoiceForm = this.buildForm();

  constructor(
    private readonly router: Router,
    private readonly snackBar: MatSnackBar,
    private readonly newInvoiceService: NewInvoiceService,
  ) {}

  private buildForm() {
    return new FormGroup({
      invoiceItems: new FormArray<FormGroup<InvoiceItemForm>>([this.createInvoiceItemFormGroup()]),
    });
  }

  createInvoiceItemFormGroup() {
    return new FormGroup<InvoiceItemForm>({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30)]),
      count: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(100),
        Validators.pattern('^[0-9]*$')]),
      price: new FormControl(null, [
        Validators.required,
        Validators.min(1),
        Validators.max(1000000),
        Validators.pattern('^[0-9]*$')]),
    });
  }

  removeInvoiceItemFormGroup(i: number) {
    this.newInvoiceForm.controls.invoiceItems.removeAt(i);
  }

  addInvoiceItemFormGroup() {
    this.newInvoiceForm.controls.invoiceItems.push(this.createInvoiceItemFormGroup());
  }

  onSubmit() {
    this.newInvoiceForm.updateValueAndValidity();
    this.newInvoiceForm.markAllAsTouched();

    if(this.newInvoiceForm.controls.invoiceItems.length === 0) {
      this.snackBar.open('Please add items', 'OK', {duration: 3000});
      return;
    }

    if(this.newInvoiceForm.invalid) {
      return;
    }

    this.newInvoiceService.saveInvoice(this.newInvoiceForm.controls.invoiceItems.value as InvoiceItem[]);
    this.router.navigate(['/invoice-preview']);
  }
}
