import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {NewInvoiceService} from '../../services/new-invoice.service';
import {CompanyApiService} from '../../services/company-api.service';

@Component({
  selector: 'app-invoice-preview',
  templateUrl: './invoice-preview.component.html',
  styleUrls: ['./invoice-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InvoicePreviewComponent implements OnDestroy {
  readonly displayedColumns = ['no', 'item', 'count', 'price'];
  companyInfo = this.companyApiService.getCompanyInfo();
  newInvoice = this.newInvoiceService.newInvoice;
  totalValue = this.calculateTotalValue();

  constructor(
    private readonly newInvoiceService: NewInvoiceService,
    private readonly companyApiService: CompanyApiService,
  ) { }

  ngOnDestroy(): void {
    this.newInvoiceService.removeInvoice();
  }

  private calculateTotalValue(): number {
    if(!this.newInvoice.length) {
      return 0;
    }
    return this.newInvoice.map(item=>item.price * item.count).reduce((a, b)=>a + b);
  }
}
