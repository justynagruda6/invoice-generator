import {Pipe, PipeTransform} from '@angular/core';
import {FormControl} from '@angular/forms';
import {map, merge, Observable, startWith} from 'rxjs';

@Pipe({
  name: 'errorMessage',
})
export class ErrorMessagePipe implements PipeTransform {

  transform(control: FormControl): Observable<string> {
    return merge(
      control.valueChanges,
      control.statusChanges,
    ).pipe(
      startWith(null),
      map(()=>{
        switch(true) {
          case control.hasError('required'):
            return 'This field is required';
          case control.hasError('minlength'):
            return `Minimum length is ${control.errors?.['minlength']?.requiredLength}`;
          case control.hasError('maxlength'):
            return `Maximum length is ${control.errors?.['maxlength']?.requiredLength}`;
          case control.hasError('min'):
            return `Minimum value is ${control.errors?.['min']?.min}`;
          case control.hasError('max'):
            return `Maximum value is ${control.errors?.['max']?.max}`;
          case control.hasError('pattern'):
            return 'Invalid pattern';
          default:
            return '';
        }
      }),
    );
  }

}
