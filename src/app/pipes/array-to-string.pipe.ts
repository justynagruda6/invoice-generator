import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'arrayToString',
})
export class ArrayToStringPipe implements PipeTransform {

  transform(array: string[]): string {
    return array.join(', ');
  }

}
