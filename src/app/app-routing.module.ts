import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InvoicePreviewComponent} from './components/invoice-preview/invoice-preview.component';
import {NewInvoiceFormComponent} from './components/new-invoice-form/new-invoice-form.component';

const routes: Routes = [
  {
    path: 'new-invoice',
    component: NewInvoiceFormComponent,
  },
  {
    path: 'invoice-preview',
    component: InvoicePreviewComponent,
  },
  {
    path: '',
    redirectTo: '/new-invoice',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/new-invoice',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
