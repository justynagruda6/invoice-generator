import {Injectable} from '@angular/core';
import {InvoiceItem} from '../models/invoice-item.model';

@Injectable({
  providedIn: 'root',
})
export class NewInvoiceService {
  newInvoice: InvoiceItem[] = [];

  saveInvoice(document: InvoiceItem[]): void {
    this.newInvoice = document;
  }

  removeInvoice(): void {
    this.newInvoice = [];
  }
}
