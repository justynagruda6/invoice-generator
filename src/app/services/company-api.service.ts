import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CompanyInfo} from '../models/company-info.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CompanyApiService {
  constructor(private http: HttpClient) { }

  getCompanyInfo(): Observable<CompanyInfo> {
    return this.http.get<any>(`../../../assets/company.json`);
  }
}
